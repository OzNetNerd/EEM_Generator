# EEM Generator

The EEM Generator program is used to create implementation and rollback scripts. It is best used when you need to make changes that will cause you to lose connectivity to a device, e.g changing a management IP address. By using EEM in this way, you no longer have to rely on the extremely disruptive and time consuming `reload in <x>` command.

EEM Generator can also be used in more complex situations too. For example, say you wanted to perform the following tasks on a remote router:

1. Bounce the WAN interface.
2. Change the WAN interface's IP address.
3. Change the BGP ASN on the WAN interface.

If you tried doing any of these tasks through an SSH session, you would lose remote connectivity. However, as EEM commands are run locally you can make all three of these changes simultaneously. If the changes does not work for any reason (e.g a misconfiguration on your ISP's side) and you lose connectivity to the router, the EEM script will automatically revert the configuration back to its original state, restoring your remote access as a result. 
## Inputs

* **-n CHANGE_NAME**: Name of the change **(Required)**
* **-c CHANGE_FILE**: Filename containing the change commands **(Optional)**
* **-b BACKOUT_FILE**: Filename containing the backout commands **(Required)**
* **-t BACKOUT_TIME**: Number of seconds to wait before backout script is run **(Optional)**
* **-o OUTPUT_FILE**: Filename for the generated EEM script(s) **(Optional - DEFAULT: EEM_Generator.txt)**
                 
## Example

In this example, the management IP address on Vlan1 needs to be changed from 10.0.0.1  to 192.168.0.1. The "change.txt" file contains the commands requried to complete the change while the "backout.txt" file contians the commands which are required to backout the change.

**change.txt**

```
enable
config t
int fa0/0
desc New management IP
ip address 192.168.0.1 255.255.255.0
```

**backout.txt**

```enable
conf t
int fa0/0
desc Original management IP
ip address 10.0.0.1 255.255.255.0
```

To generate the EEM script, use the following command:

```python2.7 EEM.py -n "Management IP Change" -c change.txt -b backout.txt -t 50 -o config.txt```

Doing so produces the following output:

```
!==============================
! IMPLEMENTATION INSTRUCTIONS
!==============================
! 1) Paste *both* the 'Change' and 'Backout' scripts onto the device.
! 2) Once you're done, run the 'Script removal' commands to remove the scripts from the device.

! Note: By default, the 'Change' script runs after 10 seconds by default. To avoid issues, please ensure *both*
! the 'Change' and 'Backout' scripts are pasted into the device within 10 seconds.

! Also note that the 'Backout' script will automatically run in 50 seconds. If you are happy with your change and
! do not want it rolled back, run the 'Script Removal' command(s) before this timer expires.


!=========================
! CHANGE SCRIPT
!=========================
event manager applet Management_IP_Change
 event timer countdown name IMPLEMENTATION_Management_IP_Change_COUNTDOWN time 10
 action 1000 cli command "enable"
 action 1010 cli command "config t"
 action 1020 cli command "int fa0/0"
 action 1030 cli command "desc New management IP"
 action 1040 cli command "ip address 192.168.0.1 255.255.255.0"

!=========================
! BACKOUT SCRIPT
!=========================
event manager applet BACKOUT_Management_IP_Change
 event timer countdown name BACKOUT_Management_IP_Change_COUNTDOWN time 50
 action 1000 cli command "enable"
 action 1010 cli command "conf t"
 action 1020 cli command "int fa0/0"
 action 1030 cli command "desc Original management IP"
 action 1040 cli command "ip address 10.0.0.1 255.255.255.0"

!=========================
! SCRIPT REMOVAL
!=========================
no event manager applet Management_IP_Change
no event manager applet BACKOUT_Management_IP_Change
```

As per the instructions above, all we need to do now is copy and paste both the "Change" and "Backout" scripts into our device. If the change was successful we would then run the "Script Removal" commands on the device before the timer expired. If the change was unsuccessful and we lost connectivity to the device, the 'Backout' script would automatically run in 50 seconds, causing the change to be rolled back.