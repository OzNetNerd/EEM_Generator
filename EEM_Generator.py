#!/usr/bin/env python

# Author: Will Robinson
# Website: oznetnerd.com
# E-Mail: will@oznetnerd.com

import argparse

"""function to create heads for each section of output"""
def heading(num,heading):
        script_output.write("!" + "=" * num)
        script_output.write("\n! " + heading + "\n")
        script_output.write("!" + "=" * num + "\n")

version = "v0.1"

"""create parser"""
parser = argparse.ArgumentParser(description='EEM Script Generator')

"""script version"""
parser.add_argument('-v', action='version', version='%(prog)s ' + version)

"""required - obtain the name of the change"""
parser.add_argument('-n', action='store', dest='change_name', required=True, help='Required: Name of the change')

"""optional - change filename"""
parser.add_argument('-c', action='store', default='NONE', dest='change_file', required=False, help='Optional: Filename containing the change commands')

"""optional - backout filename"""
parser.add_argument('-b', action='store', dest='backout_file', required=True, help='Required: Filename containing the backout commands')

"""optional - backout timer"""
parser.add_argument('-t', action='store', default=60, dest='backout_time', required=False, type=int, help='Optional: Number of seconds to wait before backout script is run')

"""optional - script output filename"""
parser.add_argument('-o', action='store', default='EEM_Generator.txt', dest='output_file', required=False, help='Optional: Filename for the generated EEM script(s) (DEFAULT: EEM_Generator.txt)')

results = parser.parse_args()

"""convert above vars to more usable names"""
change_name = results.change_name.replace(" ", "_")
change_file = results.change_file
backout_file = results.backout_file
output_file = results.output_file
backout_time = results.backout_time

"""create backout applet name"""
backout_name = "BACKOUT_" + change_name

"""confirm change_file exists"""
try:
        if change_file.upper() != "NONE":
                open(change_file, 'r')

#if change file does not exist, display error message and exit
except IOError:
        print("ERROR: " + change_file + " does not exist. Please try again.")
	sys.exit(0)

"""confirm backout_file exists"""
try:
	open(backout_file, 'r')

except IOError:
        print("ERROR: " + backout_file + " does not exist. Please try again.")
	sys.exit(0)

"""code required to open output_file"""
script_output = open(output_file, 'w')

"""variable for EEM "action" count"""
i = 1000

"""Print instructions to output_file"""
heading(30,"IMPLEMENTATION INSTRUCTIONS")
script_output.write("! 1) Paste *both* the 'Change' and 'Backout' scripts onto the device.\n")
script_output.write("! 2) Once you're done, run the 'Script removal' commands to remove the scripts from the device.\n")
script_output.write("\n! Note: By default, the 'Change' script runs after 10 seconds by default. To avoid issues, please ensure *both* \n\
! the 'Change' and 'Backout' scripts are pasted into the device within 10 seconds.\n\n")
script_output.write("! Also note that the 'Backout' script will automatically run " + str(backout_time) + " seconds after the implementation script is run. \n\
! If you are happy with your change and do not want it rolled back, run the 'Script Removal' command(s) before this timer expires.\n\n")

script_output.write("\n")

"""configure implementation timer name"""
imp_timer = change_name + "_TIMER"

"""set the number of seconds to wait before running the implementation script"""
imp_secs = 10

"""configure backout timer name"""
back_timer = "B_" + change_name + "_TIMER"

"""add the implementation timer (imp_secs) to the backout timer specified by the user"""
backout_time = backout_time + imp_secs

"""if change filename was given, run this code"""
if change_file.upper() != "NONE":

        """open change_file and output the following results to output_file"""
        with open(change_file, 'r') as f:
		heading(25,"CHANGE SCRIPT")
                script_output.write("event manager applet " + change_name  + "\n")
                script_output.write(" event timer countdown name " + imp_timer + " time " + str(imp_secs) + "\n")

                """For each line in the "change_file" (f), assign the line (command) to  to 'l'.
                Next, wrap the line in the EEM command. The newline from each line is removed
                so that EEM's ' character can be added to the line. A newline is then added."""
                for l in f:
                        script_output.write(' action ' + str(i) + ' cli command "' + l.rstrip('\n') + '"\n')
                        i+=10

                """reset the variable for the EEM "action" count"""
                i = 1000

"""put a blank line between the 'Change' and 'Removal' scripts"""
script_output.write("\n")

"""open the backout_file and output the following results to output_file"""
with open(backout_file, 'r') as f:
	heading(25,"BACKOUT SCRIPT")
        script_output.write("event manager applet " + backout_name  + "\n")

	"""add the countdown timer to the EEM script"""
        script_output.write(" event timer countdown name " + back_timer + " time " + str(backout_time)  + "\n")

        """For each line in the "backout_file" (f),  assign the line (command) to  to 'l'.
        Next, wrap the line in the EEM command. The newline from each line is removed
        so that EEM's ' character can be added to the line. A newline is then added."""
        for l in f:
                script_output.write(' action ' + str(i) + ' cli command "' + l.rstrip('\n') + '"\n')

                i+=10

	"""add a blank line between backout script & script removal"""
	script_output.write("\n")


	"""create Script Removal script"""
	heading(25,"SCRIPT REMOVAL")

	"""if a change file was use, generate removal line for it"""
	if change_file.upper()!= "NONE":
        	script_output.write("no event manager applet " + change_name  + "\n")

	"""generate removal line for backout file"""
        script_output.write("no event manager applet " + backout_name  + "\n")

try:
        open(output_file, 'r')
        print "\n" + output_file + " created successfully.\n"
except IOError:
        print "Error creating " + output_file + "."
